# nette-mail

MIME-compliant email creation and transfer https://nette.org

[![PHPPackages Rank](http://phppackages.org/p/nette/mail/badge/rank.svg)](http://phppackages.org/p/nette/mail)
[![PHPPackages Referenced By](http://phppackages.org/p/nette/mail/badge/referenced-by.svg)](http://phppackages.org/p/nette/mail)